# vimrc editing
alias evim='vim ~/.vim/settings/*'
alias evundles='vim ~/.vim/vundles/*'
alias ezsh='vim ~/.zsh/settings/*'
alias ekarabiner='vim ~/.dotfiles/karabiner/*'
alias eeditor='vim ~/.editorconfig'
alias ebrew='vim ~/.dotfiles/Brewfile'
alias egig='vim ~/.dotfiles/git/ignore'

alias elocal='vim ~/.local/**/*.zshrc'
alias ebookmark='vim ~/.local/zsh/bookmarks.zshrc'
