let g:indent_guides_auto_colors = 0
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

hi IndentGuidesOdd  guibg=red   ctermbg=10
hi IndentGuidesEven guibg=green ctermbg=0
