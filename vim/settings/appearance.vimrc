"turn on syntax highlighting
syntax on

" left gutter
set number
set relativenumber

"Disable cursor blink
set gcr=a:blinkon0

" Display tabs and trailing spaces visually
	set list listchars=tab:\⋙\ ,trail:·,eol:¬

set wrap
set linebreak

silent! colorscheme solarized
set background=dark
