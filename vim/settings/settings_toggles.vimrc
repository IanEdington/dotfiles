" ===== toggle settings =====
" cow instead (unimpaired)
" noremap <leader>tw :set wrap!<CR>
" inoremap <leader>tw <esc>:set wrap!<CR>i

" coh instead (unimpaired)
" noremap <leader>th :set hlsearch!<CR>
" inoremap <leader>th <esc>:set hlsearch!<CR>i

" con instead (unimpaired)
" noremap <leader>tn :set number!<CR>
" inoremap <leader>tn <esc>:set number!<CR>i

" cor instead (unimpaired)
" noremap <leader>trn :set relativenumber!<CR>
" inoremap <leader>trn <esc>:set relativenumber!<CR>i

" Syntastic cot instead (unimpaired)
noremap cot :SyntasticToggleMode<CR>

" set fold column
noremap cof :set foldcolumn=4
noremap cof :set foldcolumn=0
