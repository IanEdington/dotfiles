"Faster search of content and plugin folders
noremap <leader>ip :CtrlP web/app/plugins<CR>
noremap <leader>it :CtrlP web/app/themes<CR>
